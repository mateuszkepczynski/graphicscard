public interface GraphicsSetting {
   void getNeededProcessingPower();
   void processFrame();

}
