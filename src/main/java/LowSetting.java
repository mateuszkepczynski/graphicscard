public class LowSetting implements GraphicsSetting {
    @Override
    public void getNeededProcessingPower() {
        System.out.println("250");
    }

    @Override
    public void processFrame() {
        System.out.println("250");
    }
}
