public class GraphicsCard {

    private GraphicsSetting graphicsSetting;


    public GraphicsCard() {

        this.graphicsSetting = new MediumSetting();

    }

    public void getNeededProcessingPower() {
        graphicsSetting.getNeededProcessingPower();
    }

    public void processFrame() {
        graphicsSetting.processFrame();
    }

    public GraphicsSetting getGraphicsSetting() {
        return graphicsSetting;
    }

    public void setGraphicsSetting(GraphicsSetting graphicsSetting) {
        this.graphicsSetting = graphicsSetting;
    }
}
