import java.awt.*;

public class Main {

    public static void main(String[] args) {
       GraphicsCard graphicsCard = new GraphicsCard();
       graphicsCard.getNeededProcessingPower();
       graphicsCard.processFrame();
       graphicsCard.setGraphicsSetting(new HDSetting());
        graphicsCard.getNeededProcessingPower();
        graphicsCard.processFrame();
        graphicsCard.setGraphicsSetting(new LowSetting());
        graphicsCard.getNeededProcessingPower();
        graphicsCard.processFrame();
        Color newColor= new Color();
        System.out.println(newColor.getRed());
    }

}
